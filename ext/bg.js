function clickHandler() {
    var userName = document.getElementById("userName").value;
    var urlLink = "http://www.twitch.tv/"+userName+"/chat?popout=";
    chrome.tabs.create({ url: urlLink });
}

function enterHandler(e) {
    if(e.keyCode == 13){
        var userName = document.getElementById("userName").value;
        var urlLink = "http://www.twitch.tv/"+userName+"/chat?popout=";
        chrome.tabs.create({ url: urlLink });
    }
    else{
        return;
    }
}

document.getElementById('getVars').onclick = clickHandler;
document.getElementById('userName').onkeypress = enterHandler;